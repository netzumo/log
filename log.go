package log

// Default global variable
var Default, _ = New(nil)

// SetOptions set the log options
func SetOptions(opts *Options) error {
	return Default.SetOptions(opts)
}

// Print log function
func Print(args ...interface{}) {
	Default.Print(args...)
}

// Printf log function
func Printf(format string, args ...interface{}) {
	Default.Printf(format, args...)
}

// Debug log function
func Debug(args ...interface{}) {
	Default.Debug(args...)
}

// Debugf log function
func Debugf(format string, args ...interface{}) {
	Default.Debugf(format, args...)
}

// Info log function
func Info(args ...interface{}) {
	Default.Info(args...)
}

// Infof log function
func Infof(format string, args ...interface{}) {
	Default.Infof(format, args...)
}

// Warn log function
func Warn(args ...interface{}) {
	Default.Warn(args...)
}

// Warnf log function
func Warnf(format string, args ...interface{}) {
	Default.Warnf(format, args...)
}

// Error log function
func Error(args ...interface{}) {
	Default.Error(args...)
}

// Errorf log function
func Errorf(format string, args ...interface{}) {
	Default.Errorf(format, args...)
}

// WithField add a single field to the log.
func WithField(key string, value interface{}) *Log {
	return Default.WithField(key, value)
}

// WithFields add a map of fields to the log.
func WithFields(m M) *Log {
	return Default.WithFields(m)
}
