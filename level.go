package log

import "fmt"

// Level type
type Level uint8

const (
	// DebugLevel level. Usually only enabled when debugging. Very verbose logging.
	DebugLevel Level = iota
	// InfoLevel level. General operational entries about what's going on inside the
	// application.
	InfoLevel
	// WarnLevel level. Non-critical entries that deserve eyes.
	WarnLevel
	// ErrorLevel level. Logs. Used for errors that should definitely be noted.
	ErrorLevel
)

// String convert the Level to a string.
func (level Level) String() string {
	switch level {
	case DebugLevel:
		return "debug"
	case InfoLevel:
		return "info"
	case WarnLevel:
		return "warning"
	case ErrorLevel:
		return "error"
	}

	return "unknown"
}

// IsValid check if level is a valid value
func (level Level) IsValid() bool {
	if level >= DebugLevel && level <= ErrorLevel {
		return true
	}
	return false
}

// ParseLevel takes a string level and returns log level constant.
func ParseLevel(lvl string) (Level, error) {
	switch lvl {
	case "error":
		return ErrorLevel, nil
	case "warn", "warning":
		return WarnLevel, nil
	case "info":
		return InfoLevel, nil
	case "debug":
		return DebugLevel, nil
	}

	var l Level
	return l, fmt.Errorf("not a valid level: %q", lvl)
}
