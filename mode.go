package log

import "fmt"

// Mode type
type Mode string

const (
	// ConsoleMode mode for logging to console.
	ConsoleMode Mode = "console"
	// FileMode mode for logging to file.
	FileMode Mode = "file"
)

// IsValid check if mode is a valid value
func (mode Mode) IsValid() bool {
	return mode == ConsoleMode || mode == FileMode
}

// ParseMode takes a string mdoe and returns log mode constant.
func ParseMode(m string) (Mode, error) {
	mode := Mode(m)
	if mode.IsValid() {
		return mode, nil
	}

	return mode, fmt.Errorf("not a valid mode: %q", mode)
}
