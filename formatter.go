package log

import "fmt"

// Formatter type
type Formatter string

const (
	// JSONFormat format for json
	JSONFormat Formatter = "json"
	// TextFormat format for text
	TextFormat Formatter = "text"
)

// IsValid check if formatter is a valid value
func (ft Formatter) IsValid() bool {
	return ft == JSONFormat || ft == TextFormat
}

// ParseFormatter takes a string mdoe and returns log formatter constant.
func ParseFormatter(m string) (Formatter, error) {
	fm := Formatter(m)
	if fm.IsValid() {
		return fm, nil
	}

	return fm, fmt.Errorf("not a valid formatter: %q", fm)
}
