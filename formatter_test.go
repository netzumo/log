package log

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFormatterIsValid(t *testing.T) {
	assert.True(t, TextFormat.IsValid())
	assert.True(t, JSONFormat.IsValid())
	assert.True(t, Formatter("text").IsValid())
	assert.True(t, Formatter("json").IsValid())
	assert.False(t, Formatter("xml").IsValid())
	assert.False(t, Formatter("").IsValid())
}

func TestParseFormatter(t *testing.T) {
	l, err := ParseFormatter("text")
	assert.Nil(t, err)
	assert.Equal(t, TextFormat, l)

	l, err = ParseFormatter("json")
	assert.Nil(t, err)
	assert.Equal(t, JSONFormat, l)

	l, err = ParseFormatter("invalid")
	assert.Equal(t, "not a valid formatter: \"invalid\"", err.Error())
}

