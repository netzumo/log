package log

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/lestrrat/go-file-rotatelogs"
)

var (
	// ErrInvalidMode error when log mode is invalid
	ErrInvalidMode = errors.New("invalid log mode")
	// ErrInvalidLevel error when log level is invalid
	ErrInvalidLevel = errors.New("invalid log level")
	// ErrInvalidFormatter error when formatter is invalid
	ErrInvalidFormatter = errors.New("invalid formatter")
)

// M wrap map[string]interface{}
type M map[string]interface{}

// Options is a set of configuration options for log
type Options struct {
	Mode         Mode
	Level        Level
	Formatter    Formatter
	LogFile      string        // only for FileMode
	LogDir       string        // only for FileMode
	RotationTime time.Duration // only for FileMode
	MaxAge       time.Duration // only for FileMode
	Offset       time.Duration // only for FileMode
}

// Log representation
type Log struct {
	logger    LogrusInterface
	level     Level
	output    io.Writer
	formatter Formatter
}

// assert Interface compliance
var _ Interface = (*Log)(nil)

// New take options to configure log and return new Log, and an error
func New(opts *Options) (*Log, error) {
	if opts == nil {
		opts = &Options{
			Mode:      ConsoleMode,
			Level:     InfoLevel,
			Formatter: TextFormat,
		}
	}

	log := &Log{}
	if err := log.SetOptions(opts); err != nil {
		return nil, err
	}
	return log, nil
}

// SetOptions set the log options
func (log *Log) SetOptions(opts *Options) (err error) {
	if !opts.Mode.IsValid() {
		return ErrInvalidMode
	}

	if !opts.Level.IsValid() {
		return ErrInvalidLevel
	}

	if !opts.Formatter.IsValid() {
		return ErrInvalidFormatter
	}

	log.level = opts.Level
	if log.output, err = getOutput(opts); err != nil {
		return
	}
	log.formatter = opts.Formatter
	log.logger = initializeExtLogger(opts.Level, log.output, opts.Formatter, opts.Mode)

	return nil
}

// DiscardOutput discard the log (go to /dev/null)
func (log *Log) DiscardOutput(opts *Options) {
	discardOutput(log)
}

// Print log function
func (log *Log) Print(args ...interface{}) {
	log.logger.Print(args...)
}

// Printf log function
func (log *Log) Printf(format string, args ...interface{}) {
	log.logger.Printf(format, args...)
}

// Debug log function
func (log *Log) Debug(args ...interface{}) {
	log.logger.Debug(args...)
}

// Debugf log function
func (log *Log) Debugf(format string, args ...interface{}) {
	log.logger.Debugf(format, args...)
}

// Info log function
func (log *Log) Info(args ...interface{}) {
	log.logger.Info(args...)
}

// Infof log function
func (log *Log) Infof(format string, args ...interface{}) {
	log.logger.Infof(format, args...)
}

// Warn log function
func (log *Log) Warn(args ...interface{}) {
	log.logger.Warn(args...)
}

// Warnf log function
func (log *Log) Warnf(format string, args ...interface{}) {
	log.logger.Warnf(format, args...)
}

// Error log function
func (log *Log) Error(args ...interface{}) {
	log.logger.Error(args...)
}

// Errorf log function
func (log *Log) Errorf(format string, args ...interface{}) {
	log.logger.Errorf(format, args...)
}

// WithField add a single field to the log.
func (log *Log) WithField(key string, value interface{}) *Log {
	return &Log{
		level:     log.level,
		output:    log.output,
		formatter: log.formatter,
		logger:    log.logger.WithField(key, value),
	}
}

// WithFields add a map of fields to the log.
func (log *Log) WithFields(m M) *Log {
	return &Log{
		level:     log.level,
		output:    log.output,
		formatter: log.formatter,
		logger:    withFields(log.logger, m),
	}
}

func initializeExtLogger(level Level, out io.Writer, ft Formatter, mode Mode) *logrus.Logger {
	lg := logrus.New()
	lg.Level = logLevelToLogrusLevel(level)
	lg.Out = out

	switch ft {
	case TextFormat:
		fm := &logrus.TextFormatter{}
		if mode == FileMode {
			fm.DisableColors = true
		}
		lg.Formatter = fm
	case JSONFormat:
		lg.Formatter = &logrus.JSONFormatter{}
	}

	return lg
}

func logLevelToLogrusLevel(level Level) logrus.Level {
	if logrusLvl, err := logrus.ParseLevel(level.String()); err != nil {
		panic(fmt.Sprintf("Logrus: %v", err))
	} else {
		return logrusLvl
	}
}

func withFields(l LogrusInterface, m M) LogrusInterface {
	return l.WithFields((logrus.Fields)(m))
}

// getOutput return io writer based on mode
func getOutput(opts *Options) (io.Writer, error) {
	switch opts.Mode {
	case ConsoleMode:
		return os.Stdout, nil
	case FileMode:
		logFile := opts.LogFile

		if logFile == "" {
			logFile = "application.log"
		}

		logDir := opts.LogDir

		if logDir == "" {
			logDir = "log"
		}

		// get the abs path to current dir
		currentDir, _ := filepath.Abs(".")
		logDir = makeAbsolute(logDir, currentDir)
		if err := os.MkdirAll(logDir, os.ModePerm); err != nil {
			return nil, fmt.Errorf("cannot create log dir '%s': %v", logDir, err)
		}

		rl := rotatelogs.NewRotateLogs(logDir + "/" + logFile + ".%Y%m%d%H%M")
		rl.LinkName = logDir + "/" + logFile
		rl.RotationTime = opts.RotationTime
		rl.MaxAge = opts.MaxAge
		rl.Offset = opts.Offset
		return rl, nil
	}

	return nil, ErrInvalidMode
}

func discardOutput(log *Log) {
	log.output = ioutil.Discard
	lg := logrus.New()
	lg.Out = log.output
	log.logger = lg
}

// return absolute path of given path, use root is the path is relative
func makeAbsolute(path, root string) string {
	if filepath.IsAbs(path) {
		return path
	}
	return filepath.Join(root, path)
}
