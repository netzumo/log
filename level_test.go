package log

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConvertLevelToString(t *testing.T) {
	assert.Equal(t, "debug", DebugLevel.String())
	assert.Equal(t, "info", InfoLevel.String())
	assert.Equal(t, "warning", WarnLevel.String())
	assert.Equal(t, "error", ErrorLevel.String())
}

func TestParseLevel(t *testing.T) {
	l, err := ParseLevel("error")
	assert.Nil(t, err)
	assert.Equal(t, ErrorLevel, l)

	l, err = ParseLevel("warn")
	assert.Nil(t, err)
	assert.Equal(t, WarnLevel, l)

	l, err = ParseLevel("warning")
	assert.Nil(t, err)
	assert.Equal(t, WarnLevel, l)

	l, err = ParseLevel("info")
	assert.Nil(t, err)
	assert.Equal(t, InfoLevel, l)

	l, err = ParseLevel("debug")
	assert.Nil(t, err)
	assert.Equal(t, DebugLevel, l)

	l, err = ParseLevel("invalid")
	assert.Equal(t, "not a valid level: \"invalid\"", err.Error())
}

func TestLogLevelIsValid(t *testing.T) {
	assert.True(t, DebugLevel.IsValid())
	assert.True(t, InfoLevel.IsValid())
	assert.True(t, WarnLevel.IsValid())
	assert.True(t, ErrorLevel.IsValid())
	assert.False(t, Level(4).IsValid())
	assert.False(t, Level(99).IsValid())
}
