package log

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestModeIsValid(t *testing.T) {
	assert.True(t, ConsoleMode.IsValid())
	assert.True(t, FileMode.IsValid())
	assert.True(t, Mode("console").IsValid())
	assert.True(t, Mode("file").IsValid())
	assert.False(t, Mode("tree").IsValid())
	assert.False(t, Mode("").IsValid())
}

func TestParseMode(t *testing.T) {
	l, err := ParseMode("console")
	assert.Nil(t, err)
	assert.Equal(t, ConsoleMode, l)

	l, err = ParseMode("file")
	assert.Nil(t, err)
	assert.Equal(t, FileMode, l)

	l, err = ParseMode("invalid")
	assert.Equal(t, "not a valid mode: \"invalid\"", err.Error())
}
